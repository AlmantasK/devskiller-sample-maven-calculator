Java - Sample programming task
------

Implement all methods within the `com.devskiller.calculator.Calculator` class. You have to implement the following mathematical operations:

- Add
- Subtract
- Multiply
- Divide

Methods should throw `IllegalArgumentException` for invalid arguments.

You can check unit tests in `com.devskiller.calculator.BaseTest` for detailed requirements.
